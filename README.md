# ML Ideas
ML(<inputs>, <output>)

## Audio Rec
The Goal is to get one set of audio to sound like another

### How:
Use audio array to get a wave pattern that describes how the sample looks like

Trained like so:
ML(<wave pattern>, <original pattern>)

then any new audio that has a similar wave apperance will return the same output of the original training audio







