from scipy.io.wavfile import read, write
from knearest import Brain
import numpy as np
import wave
from simplification.cutil import simplify_coords
from time import time

import matplotlib.pyplot as plt

brain_name = 'audio_brain.sav'

SAMPLE_RATE = 0
SPLITS = 500

INC = 500
IDX = 0

def open_wav(filename):
    """Opening wav file to array between (-1, 1). If wav is sterio convert to mono"""
    global SAMPLE_RATE
    print("Reading wav file... ")
    ifile = read(filename)
    SAMPLE_RATE = ifile[0]
    wav_data = ifile[1]
    if isinstance(ifile[1][0], np.ndarray):
        audiodata = ifile[1].astype(float)
        wav_data = audiodata.sum(axis=1) / 2
    return wav_data

def normalize_array(array):
    """Returns the array where every element is between (-1, 1)."""
    return array / max(array)

def partition_array(array, pieces=500):
    """Splits 1D array into equal sized arrays of n size."""
    array_len = len(array)
    pieces = (array_len // 500)
    return np.array_split(array, pieces)

def reduce_2darray(array, epsilon=0.01):
    """Reduces an array using the RDP alogrithm. returning a pattern that represents the same data with fewer points"""
    reduced_2d_array = []
    for array_sector in array:
        two_d_array_sector = []
        idx = 0
        for s in array_sector:
            two_d_array_sector.append([idx, s])
            idx += 1
        simplified_2d_sector = simplify_coords(two_d_array_sector, epsilon)
        reduced_2d_array.append(simplified_2d_sector)
    
    reduced_array = []
    for sector in reduced_2d_array:
        reduced_sector = []
        for coord in sector:
            reduced_sector.append(coord[1]) 
        reduced_array.append(reduced_sector)
    return reduced_array

def reduce_array(array_sector, epsilon=0.01):
    """Reduces an array using the RDP alogrithm. returning a pattern that represents the same data with fewer points"""
    reduced_2d_array = []
    two_d_array_sector = []
    idx = 0
    for s in array_sector:
        two_d_array_sector.append([idx, s])
        idx += 1
    simplified_2d_sector = simplify_coords(two_d_array_sector, epsilon)
    reduced_2d_array.append(simplified_2d_sector)
    reduced_array = []
    for sector in reduced_2d_array:
        reduced_sector = []
        for coord in sector:
            reduced_sector.append(coord[1]) 
        reduced_array.append(reduced_sector)
    return reduced_array

def do_audio_shit():
    """Opening wav file to array between (-1, 1)"""

    audio_data = open_wav(input_audio)
    audio_normalised = normalize_array(audio_data)
    partitioned_array = partition_array(audio_normalised, SPLITS)
    reduced_array = reduce_2darray(partitioned_array, EPSILON)
    assert len(partitioned_array) == len(reduced_array), \
        f"Expected partitioned array to be the same size as the reduced array"
    partitioned_array = partition_array(audio_data, SPLITS)
    return reduced_array, partitioned_array

def extend_array(array, desired_length):
    new_array = array
    while len(new_array) < desired_length:
        tmp_array = []
        for a in new_array:
            if len(tmp_array) == desired_length:
                break
            tmp_array.append(a)
            tmp_array.append(a)
        new_array = tmp_array
    return new_array


def train():
    global IDX, INC, INPUT_AUDIO
    print("DOING TRAINING")
    print("Doing Brain stuff... ")
    
    outputs = open_wav(INPUT_AUDIO)
    
    b = Brain(INC, 1)
    pn = -1
    pt = time()
    bt = time()
    IDX = 0
    while IDX + INC < len(outputs):
        n = round(((IDX + INC)/len(outputs)) * 100)
        if n != pn:
            print(f"Progress {n}% {round(time() - pt, 1)}s - {round(time()-bt, 1)}s total\t{IDX} / {len(outputs)}")
            pn = n
            pt = time()
        output = outputs[IDX:IDX + INC]
        inputs = reduce_array(output, max(output) / 1000)[0]
        inputs = extend_array(inputs, INC)
        b.add(inputs, output)
        IDX += INC

    print("Saving brain")
    b.save_brain(brain_name)
    print("Done.")


def predict():
    global IDX, INC, INPUT_AUDIO
    print("Loading brain")
    b = Brain(0, 1)
    b.load_brain(brain_name)
    print("Done.")

    print("DOING PREDICTIONS")
    print("Doing Brain stuff... ")
    print("Doing Predictions... ", end='')
    pn = -1
    pt = time()
    bt = time()
    predictions = []
    outputs = open_wav(INPUT_AUDIO)
    IDX = 0
    while IDX + INC < len(outputs):
        n = round(((IDX + INC)/len(outputs)) * 100)
        if n != pn:
            print(f"Progress {n}% {round(time() - pt, 1)}s - {round(time()-bt, 1)}s total")
            pn = n
            pt = time()
        output = outputs[IDX:IDX + INC]
        inputs = reduce_array(output, max(output) / 1000)[0]
        inputs = extend_array(inputs, INC)
        predictions.extend(b.predict(inputs).tolist())
        IDX += INC
    predictions = np.array(predictions, dtype=np.int16)

    print("Done")

    try:
        write('output.wav', int(SAMPLE_RATE), predictions)
    except Exception as e:
        print("error!", e)
        breakpoint()
    print("Done.")

INPUT_AUDIO = 'JEB_OUTPUT.wav'
INPUT_AUDIO = 'big_test.wav'
train()
INPUT_AUDIO = 'jb.wav'
predict()


# inputs = open_wav(input_audio)
# print('Plotting')
# # breakpoint()
# plt.plot(inputs[IDX:IDX + INC])
# plt.show()
# # breakpoint()
# arr = reduce_array(inputs[IDX:IDX + INC], max(inputs) / 1000)
# plt.plot(arr[0])
# plt.show()

"""Creates noisy wav."""
# data = np.random.uniform(-1,1,44100) # 44100 random samples between -1 and 1
# scaled = np.int16(data/np.max(np.abs(data)) * 32767)
